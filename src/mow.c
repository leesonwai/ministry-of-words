/* mow.c
 *
 * Copyright 2020 Joshua Lee <lee.son.wai@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <ctype.h>
#include <errno.h>
#include <getopt.h>
#include <libgen.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "config.h"

#define CHUNKSIZ            32
#define MOBYSIZE            30260
#define NCOMMON             1000
#define OPTS                "hv"

struct entry
{
  char         *word;
  char         *syn;
  struct entry *next;
};

static void     die             (const char *fmt,
                                 ...);

static          struct entry    *common_words[NCOMMON];
static const    char            *prog;
static          struct entry    *thesaurus[MOBYSIZE];

static unsigned int
hash (char *word)
{
  unsigned int hashval;

  for (hashval = 0; *word; ++word)
    hashval += tolower (*word) + 31 * hashval;

  return hashval;
}

static struct entry *
lookup (struct entry **table,
        char          *word,
        int            size)
{
  struct entry *e;

  for (e = table[hash (word) % size]; e; e = e->next)
    {
      if (!strcasecmp (word, e->word))
        return e;
    }

  return NULL;
}

static struct entry *
install (struct entry **table,
         char          *word,
         char          *syn,
         int            size)
{
  struct entry *e;
  unsigned int hashval;

  if (!(e = lookup (table, word, size)))
    {
      if (!(e = malloc (sizeof (struct entry))) || !(e->word = strdup (word)))
        die ("%s: %s\n", prog, strerror (errno));

      hashval = hash (word) % size;
      e->next = table[hashval];
      table[hashval] = e;
    }
  else
    {
      free (e->syn);
    }

  if (!(e->syn = strdup (syn)))
    die ("%s: %s\n", prog, strerror (errno));

  return e;
}

static void
tablefree (struct entry **table,
           int            size)
{
  struct entry *head;
  struct entry *tmp;

  for (int i = 0; i < size; ++i)
    {
      head = table[i];

      while (head)
        {
          tmp = head;
          head = head->next;
          free (tmp->word);
          free (tmp->syn);
          free (tmp);
        }
    }
}

static char *
proc (char *word)
{
  struct entry *e = lookup (thesaurus, word, MOBYSIZE);

  return (e) ? e->syn : word;
}

static int
getword (char **lineptr)
{
  char *buf;
  int c;
  int len = 0;
  int bufm = 1;

  if (!(buf = malloc (CHUNKSIZ)))
    die ("%s: %s\n", prog, strerror (errno));

  buf[1] = '\0';

  if ((c = getchar ()) == EOF)
    return -1;

  if (!isalnum (c))
    {
      *buf = c;
      *lineptr = buf;
      return 1;
    }

  buf[len++] = c;
  while (!isspace (c = getchar ()))
    {
      buf[len++] = c;
      if (len == CHUNKSIZ * bufm - 1)
        {
          ++bufm;
          if (!(buf = realloc (buf, CHUNKSIZ * bufm)))
            die ("%s: %s\n", prog, strerror (errno));
        }
    }
  buf[len] = '\0';

  *lineptr = buf;
  ungetc (c, stdin);

  return len;
}

static void
help (void)
{
  printf ("Usage: %s [OPTION]\n"
          "\"Never use a long word where a short one will do.\"\n"
          "\n  -h\tPrint help\n"
          "  -v\tPrint program version\n"
          "\nmow reads from stdin and writes to stdout.\n", prog);
}

static void
mkthesaurus (const char *path)
{
  FILE *fp;
  int len;
  char *line = NULL;
  size_t n = 0;

  if (!(fp = fopen (path, "r")))
    die ("%s: %s\n", prog, strerror (errno));

  while ((len = getline (&line, &n, fp)) != -1)
    {
      line[len - 1] = '\0';

      for (char *word = strtok (line, ","), *buf = word; buf; buf = strtok (NULL, ","))
        {
          if (lookup (common_words, buf, NCOMMON))
            {
              install (thesaurus, word, buf, MOBYSIZE);
              break;
            }
        }
      free (line);
      line = NULL;
    }

  fclose (fp);
}

static void
mkcommon (const char *path)
{
  FILE *fp;
  int len;
  char *buf = NULL;
  size_t n = 0;

  if (!(fp = fopen (path, "r")))
    die ("%s: %s\n", prog, strerror (errno));

  while ((len = getline (&buf, &n, fp)) != -1)
    {
      buf[len - 1] = '\0';
      install (common_words, buf, buf, NCOMMON);
      free (buf);
      buf = NULL;
    }

  fclose (fp);
}

static char *
getpath (const char *file)
{
  char *env;
  char *data_dirs;
  char *dir;
  char *path;

  if (!(env = getenv ("XDG_DATA_DIRS")))
    env = "/usr/local/share/:/usr/share/";

  data_dirs = strdup (env);

  for (dir = strtok (data_dirs, ":"); dir; dir = strtok (NULL, ":"))
    {
      if (!(path = malloc (strlen (dir) + strlen ("mow/") + strlen (file) + 1)))
        die ("%s: %s\n", prog, strerror (errno));
      sprintf (path, "%s%s%s", dir, "mow/", file);

      if (access (path, F_OK) != -1)
        {
          free (data_dirs);
          return path;
        }
      free (path);
    }
  free (data_dirs);

  return NULL;
}

static void
die (const char *fmt,
     ...)
{
  va_list ap;

  va_start (ap, fmt);
  vfprintf (stderr, fmt, ap);
  va_end (ap);

  exit (EXIT_FAILURE);
}

int
main (int    argc,
      char **argv)
{
  char *path;
  char *word;

  errno = 0;
  prog = basename (*argv);

  if (argc > 2)
    die ("Usage: %s [OPTION]\n", prog);

  switch (getopt (argc, argv, OPTS))
    {
    case -1:
      break;
    case 'h':
      help ();
      return EXIT_SUCCESS;
    case 'v':
      printf ("%s %s\n", prog, VERSION);
      return EXIT_SUCCESS;
    default:
      printf ("Try '%s -h' for more information.\n", prog);
      return EXIT_FAILURE;
    }

  /*
   * If the file doesn't exist in $XDG_DATA_DIRS/mow/,
   * assume that we're running from builddir.
   */
  if (!(path = getpath ("common.txt")))
    path = strdup ("../data/common.txt");
  mkcommon (path);
  free (path);

  if (!(path = getpath ("moby.txt")))
    path = strdup ("../data/moby.txt");
  mkthesaurus (path);
  free (path);

  tablefree (common_words, NCOMMON);

  while (getword (&word) != -1)
    {
      printf ("%s", proc (word));
      free (word);
    }

  tablefree (thesaurus, MOBYSIZE);

  return EXIT_SUCCESS;
}
