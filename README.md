# Ministry of Words
### *"Never use a long word where a short one will do."* -- George Orwell
![License: GPL3](https://img.shields.io/badge/License-GPL3+-green?style=flat-square)

Ministry of Words, or `mow`, is a simple command line utility that attempts
to simplify English language input by turning certain fancy words into their
most basic synonym using the public domain [Moby Thesaurus II](http://onlinebooks.library.upenn.edu/webbin/gutbook/lookup?num=3202).

`mow` makes no guarantees that its output is intelligible or an improvement
on its input.

`mow` is a C version of a Java assignment I was given. I decided to implement
it in C first for enjoyment; Java is not fun to write.

## Usage
`mow` reads from `stdin` and writes to `stdout`. Simply invoke it and start
typing.

## Installing
`mow` is developed and tested on Linux.

```
meson builddir
cd builddir/
ninja
sudo ninja install
```
